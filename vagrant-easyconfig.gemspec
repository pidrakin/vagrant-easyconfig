Gem::Specification.new do |s|
  s.name          = "vagrant-easyconfig"
  s.version       = "1.0.199"
  s.license       = "HOOKAHWARE"
  s.authors       = ["Daniel Satanik", "Benjamin Akhras"]
  s.email         = ["daniel@satanik.at"]
  s.homepage      = "https://gitlab.com/dessecated-unicorn/vagrant-easyconfig"
  s.summary       = "Vagrant plugin easy yaml configs"
  s.description   = "Vagrant plugin for easy VMs, provisioning and Ansible configuration."

  all_files       = `git ls-files`.split("\n")
  s.files         = all_files
  s.require_paths = ["lib"]

  s.rdoc_options     = ["--charset=UTF-8"]
  s.extra_rdoc_files = %w(README.md)

end
