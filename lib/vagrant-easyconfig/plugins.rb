# -*- mode: ruby -*-
# vi: set ft=ruby :

module EasyConfig
  module Plugins
    def self.configure(config)
      self.configure_hostmanager(config)
      self.configure_vbguest(config)
      return nil
    end

    def self.configure_hostmanager(config)
      if Vagrant.has_plugin?('vagrant-hostmanager')
        config.hostmanager.enabled            = true
        config.hostmanager.manage_host        = true
        config.hostmanager.manage_guest       = true
        config.hostmanager.ignore_private_ip  = false
        config.hostmanager.include_offline    = true
      else
        puts <<~EOL
        EasyConfig:Plugins.configure_hostmanager: no hosts plugin installed, thus no aliases are added to the /etc/hosts file
        EasyConfig:Plugins.configure_hostmanager: plugin examples: vagrant-hostmanager
        EOL
      end
      return nil
    end

    def self.configure_vbguest(config)
      if Vagrant.has_plugin?("vagrant-vbguest")
        auto_update = Config.get("plugins","vbguest","auto_update")
        config.vbguest.auto_update = auto_update != nil ? auto_update : true
        if OS.unix?
          config.vm.synced_folder ".", "/vagrant", disabled: false
        else
          config.vm.synced_folder ".", "/vagrant", disabled: false, mount_options: ["dmode=0755,fmode=0644"]
        end
      else
        puts <<~EOL
        EasyConfig:Plugins.configure_vbguest: no vagrant-vbguest plugin installed, thus no autoupdates and synced folders are working
        EasyConfig:Plugins.configure_vbguest: install with: vagrant plugin install vagrant-vbguest
        EOL
      end
      return nil
    end

    def self.configure_disksize(config)
      if Vagrant.has_plugin?("vagrant-disksize")
        config.disksize.size = Config.get("hosts", config.vm.hostname, "vm","storage")
      else
        puts <<~EOL
        EasyConfig:Plugins.configure_disksize: no disksize plugin installed, thus storage is not set correctly to #{ vm.fetch("storage") }
        EasyConfig:Plugins.configure_disksize: vagrant plugin install vagrant-disksize
        EOL
      end
      return nil
    end

    def self.add_aliases(config)
      if Vagrant.has_plugin?('vagrant-hostmanager')
        if Config.get("hosts", config.vm.hostname).key?("aliases")
          aliases = Config.get("hosts", config.vm.hostname,"aliases")
          if aliases.kind_of?(Array) && aliases.length > 0
            config.hostmanager.aliases = aliases
          end
        end
      end
      return nil
    end
  end
end
