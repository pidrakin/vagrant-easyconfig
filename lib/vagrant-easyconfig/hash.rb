# -*- mode: ruby -*-
# vi: set ft=ruby :

class ::Hash
  def deep_merge(second)
    merger = proc { |_, v1, v2| Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : Array === v1 && Array === v2 ? v1 | v2 : [:undefined, nil, :nil].include?(v2) ? v1 : v2 }
    merge(second.to_h, &merger)
  end
  def deep_merge!(second)
    merger = proc { |_, v1, v2| Hash === v1 && Hash === v2 ? v1.merge!(v2, &merger) : Array === v1 && Array === v2 ? v1 | v2 : [:undefined, nil, :nil].include?(v2) ? v1 : v2 }
    merge!(second.to_h, &merger)
  end
  def deep_update!(second)
    merger = proc { |_, v1, v2| Hash === v1 && Hash === v2 ? v1.merge!(v2.select { |k| v1.keys.include? k }, &merger) : Array === v1 && Array === v2 ? v1 | v2 : [:undefined, nil, :nil].include?(v2) ? v1 : v2 }
    merge!(second.to_h.select { |k| keys.include? k }, &merger)
  end
end
