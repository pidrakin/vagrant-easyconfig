# -*- mode: ruby -*-
# vi: set ft=ruby :

require_relative "config_template.rb"
require_relative "os.rb"
require "yaml"
require "ipaddr"

module EasyConfig
  module Config
    extend self

    @@config = {}

    def self.load(path)
      config_path = File.expand_path(path, Dir.pwd)
      vagrantfile_path = File.expand_path('Vagrantfile', Dir.pwd)
      unless File.exist? vagrantfile_path
        config_path = File.expand_path(path, File.dirname(File.dirname(File.dirname(__FILE__))))
      end
      unless File.exist? config_path
        File.write("#{config_path}.template", @@config_template)
        abort <<~EOL
        EasyConfig:Config.load: Cannot load file [#{config_path}]
        EasyConfig:Config.load: Create template [#{config_path}.template]
        EasyConfig:Config.load: Copy the template to [#{config_path}] and adapt
        EOL
      end

      self.merge_template config_path
      self.merge_default
      self.set_hostnames
      self.set_boxes
      self.auto_install_plugins
      self.initialize_ansible

      unless OS.windows?
        @@config.delete("windows")
      end

      self.post_initialize
      return nil
    end

    def self.get(*args)
      if @@config == nil
        abort "EasyConfig:Config.get: load config first with [Config.load(path)]"
      end
      if args.length == 0
        return Marshal.load(Marshal.dump(@@config))
      end
      return Marshal.load(Marshal.dump(@@config.dig(*args)))
    end

    def self.print
      if @@config == nil
        abort "EasyConfig:Config.print: load config first with [Config.load(path)]"
      end
      puts @@config.to_yaml
      puts "---\n\n"
      return nil
    end

    private

    def self.merge_template(config_path)
      template = YAML.load(@@config_template)
      # remove default host as it needs to be overridden
      template["default"]["hosts"] = {}
      file = YAML.load_file(config_path)
      @@config = template.deep_merge!(file)
    end

    def self.merge_default
      hostname = "#{`hostname`[0..-2]}"
      default = @@config.fetch("default")
      key = [hostname, hostname.downcase].detect {|key| @@config.key?(key)}
      if key and (specific = @@config.fetch(key)) != nil
        default.deep_merge!(specific)
      end
      @@config = default

      vm    = @@config.fetch("vm")
      ip    = @@config.dig("net", "ip")
      hosts = @@config.fetch("hosts")
      hosts.each_with_index do |item, index|
        hostname = item[0]
        unless item[1]
          hosts[hostname] = {}
        end
        host     = hosts[hostname]
        # create temporary copy of host
        temp     = Marshal.load(Marshal.dump(host))
        # override host with default values
        host["vm"] = host.fetch("vm", {}).deep_merge!(vm)
        # re-override host with previous values from temp
        host["vm"].deep_merge!(temp.fetch("vm", {}))

        if host.fetch("ip","") == nil or host.fetch("ip",nil) == nil
          host["ip"] = IPAddr.new(IPAddr.new(ip, Socket::AF_INET).to_i + index, Socket::AF_INET).to_s
        end
      end
      @@config.delete("vm")
      @@config["net"].delete("ip")
    end

    def self.set_hostnames
      @@config.fetch("hosts").keys.each do |guestname|

        if @@config.dig("hosts", guestname).key?("aliases")
          aliases = @@config.dig("hosts", guestname, "aliases")
          if aliases.kind_of?(Array) && aliases.length > 0
            aliases = aliases.map {|a| a = "#{`hostname`[0..-2].downcase}-#{a}"}
            @@config["hosts"][guestname]["aliases"] = aliases
          end
        end

        @@config["hosts"]["#{`hostname`[0..-2]}-#{guestname}".downcase] = @@config["hosts"][guestname]
        @@config["hosts"].delete(guestname)
      end
    end

    def self.set_boxes
  #     box = @@config.dig("vm","boxes").fetch(@@config.dig("vm","box"))
  #     @@config["vm"]["box"] = box
  #     @@config["vm"].delete("boxes")

      @@config.fetch("hosts").each do |hostname, host|
        box = host.dig("vm", "boxes").fetch(host.dig("vm","box"))
        host["vm"]["box"] = box
        host["vm"].delete("boxes")
      end
    end

    def self.auto_install_plugins
      if @@config.dig("plugins","auto_install")
        required_plugins = %w( vagrant-hostmanager vagrant-vbguest vagrant-disksize )
        _retry = false
        required_plugins.each do |plugin|
          unless Vagrant.has_plugin? plugin
            system "vagrant plugin install #{plugin}"
            _retry=true
          end
        end

        if (_retry)
          exec "vagrant " + ARGV.join(' ')
        end
      end
    end

    def self.initialize_ansible
      project_path = @@config.dig("ansible","project_path")
      if project_path == nil or project_path == ""
        project_path = "ansible"
      end
      if @@config.dig("ansible","local")
        project_path = "/vagrant/#{ project_path }".gsub(%r{/+},'/')
      end
      @@config["ansible"]["project_path"] = project_path

      playbook = @@config.dig("ansible","playbook")
      if playbook == nil or playbook == ""
        playbook = "playbook.yml"
      end
      @@config["ansible"]["playbook"] = playbook.gsub(%r{/+},'/')
    end
    def self.post_initialize
      if OS.windows?
        unless @@config.dig("windows","ssh","key") != nil and @@config.dig("windows","ssh","key") != ""
          abort <<~EOL
          EasyConfig:Config.initialize_ansible: On Windows please add [windows.ssh.key] to config.yaml
          EasyConfig:Config.initialize_ansible: and pass the absolute path to your ssh key file
          EOL
        end
        config.ssh.insert_key = false
        config.ssh.private_key_path = [@@config.dig("windows","ssh","key")]
      end
    end
  end
end
