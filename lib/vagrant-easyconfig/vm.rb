# -*- mode: ruby -*-
# vi: set ft=ruby :

module EasyConfig
  module VM
    extend self

    def self.each(config, &block)
      hostnames = Config.get("hosts").keys
      Config.get("hosts").each_with_index do |item, index|
        hostname    = item[0]
        host_config = item[1]

        config.vm.define hostname do |host|
          self.define(host, hostname, host_config, index == hostnames.size-1)

          block.call(host)
        end
      end
    end

    private

    def self.define(config, hostname, host, provision)
      ip                    = host.fetch("ip")
      type                  = Config.get("net","network_type")
      nic_type              = Config.get("net","private_nic_type")
      config.vm.network type, ip: ip, nic_type: nic_type

      config.vm.box         = host.dig("vm","box")

      config.vm.hostname    = hostname

      config.vm.provider :virtualbox do |vb|

        vb.gui              = host.dig("vm","gui")
        vb.cpus             = host.dig("vm","cpus")
        vb.memory           = host.dig("vm","memory")
        vb.name             = hostname
        # vb.default_nic_type = nic_type
        vb.linked_clone     = true if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0') and Config.get("linked_clones")
        vb.customize ["modifyvm", :id, "--nictype1", nic_type]
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]

        # workaround for Ubuntu 20.04
        if ["ubuntu/focal", "EasyConfig/ubuntu-desktop-focal-20.04-lts"].include? host.dig("vm","box")
          vb.customize ["modifyvm", :id, "--uart1", "0x3F8", "4"]
          vb.customize ["modifyvm", :id, "--uartmode1", "file", File::NULL]
        end
      end

      if Config.get("ansible","local") or provision
        playbook_file = File.expand_path(
          Config.get("ansible","playbook"),
          Config.get("ansible","project_path")
        ).gsub(%r{/vagrant/},'')
        if File.exist? playbook_file
          config.vm.provision "ansible#{ "_local" if Config.get("ansible","local") }" do |ansible|
            if Config.get("ansible","local") and Config.get("ansible","pip")
              ansible.install_mode     = "pip"
            end
            config_path = "#{Config.get("ansible","project_path")}/ansible.cfg"
            if File.exist?(config_path)
              ansible.config_file      = "#{Config.get("ansible","project_path")}/ansible.cfg"
            end
            ansible.playbook           = Config.get("ansible","playbook")
            ansible.provisioning_path  = Config.get("ansible","project_path")
            ansible.extra_vars         = Config.get("ansible","extra_vars")
            ansible.verbose            = Config.get("ansible","verbose")
            ansible.tags               = Config.get("ansible","tags")
            ansible.skip_tags          = Config.get("ansible","skip_tags")
            ansible.compatibility_mode = "2.0"

            groups = {}
            Config.get("hosts").each do |hostname, host|
              if host.key?("groups") and host.fetch("groups") != nil
                host_groups = host.fetch("groups")
                unless host_groups.kind_of?(Array)
                  host_groups = [host_groups]
                end
                host_groups.each do |group|
                  unless groups.key?(group)
                    groups[group] = []
                  end
                  groups[group] << hostname
                end
              end
            end
            ansible.groups = groups
          end
        else
          puts "EasyConfig:VM.define: no ansible playbook available at [#{playbook_file}]"
        end
      end
      Plugins.configure_disksize(config)
      Plugins.add_aliases(config)
      return nil
    end
  end
end
