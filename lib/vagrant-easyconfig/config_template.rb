module EasyConfig
  module Config
    @@config_template = <<-EOL.strip
---
default:
  vm:
    gui: false
    memory: 4096
    cpus: 4
    storage: 10GB
    # list of boxes that may be used for this project
    boxes:
      ubuntu1804: ubuntu/bionic64
      ubuntu: ubuntu/focal64
    # key for the box that is used for this project
    box: ubuntu
  hosts:
    # name of the host
    example-host:
      # if no ip is given it will be generated from net.ip
      ip:
      # groups used for ansible
      groups:
      # aliases are added to the /etc/hosts on the host and guest
      aliases:
      # override vm options per host
      # vm:
      #   memory: 8192
  plugins:
    # automatically installs vbguest, hostmanager and disksize
    auto_install: true
    vbguest:
      # turn off auto_update of vbguest
      auto_update: true
  linked_clones: true
  net:
    # ip to auto generate IPs from
    # incrementing by 1 for every host
    ip: 192.168.100.100
    private_nic_type: 82540EM
    network_type: private_network
  ansible:
    # whether to use ansible_local or ansible provisioner
    local: true
    # whether to install ansible with guest OS or pip
    pip: false
    # where the ansible project is located
    project_path:
    # where the playbook is located inside the ansible project
    playbook:
    # when used passes the extra_vars to ansible
    extra_vars:
    # when used only tasks with these tags are run
    tags:
    # when used all tasks except for one with these tags are run
    skip_tags:
    # toggle verbose logging for ansible
    verbose: false
  # will be disable on unix systems
  windows:
    ssh:
      # specify ssh-key location
      key:
# override for current host
# by specifying the hostname of the current machine
# all settings under this key are overriding the default section
#{`hostname`[0..-2].downcase}:
#  vm:
#    memory: 8192
#  hosts:
#    example-host:
#      vm:
#        memory: 16384
EOL
  end
end
