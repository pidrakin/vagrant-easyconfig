# -*- mode: ruby -*-
# vi: set ft=ruby :

begin
  require 'vagrant'
rescue LoadError
  raise 'The EasyConfig Vagrant Plugin must be run within Vagrant'
end

require_relative "vagrant-easyconfig/hash.rb"
require_relative "vagrant-easyconfig/os.rb"
require_relative "vagrant-easyconfig/plugins.rb"
require_relative "vagrant-easyconfig/config.rb"
require_relative "vagrant-easyconfig/vm.rb"
